from bson import ObjectId
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField
from wtforms.fields.html5 import EmailField, DateField, IntegerField
from wtforms.validators import DataRequired, Email

from extensions import mongo

db = mongo["user_db"]


class UserForm(FlaskForm):
    dept_choices = [(dept['_id'].__str__(), dept['dept_name']) for dept in db.depts.find({})]
    dept_choices.append(('', ' - '))
    print(dept_choices)
    # import pdb
    # pdb.set_trace()
    first_name = StringField("First name", validators=[DataRequired("This field is required")])
    last_name = StringField("Last name", validators=[DataRequired("This field is required")])
    dob = StringField("DOB", validators=[DataRequired("This field is required")])
    username = EmailField("Username", validators=[DataRequired("This field is required"), Email()])
    department = SelectField("Department", choices=dept_choices)


class DeptForm(FlaskForm):
    dept_id = IntegerField('Department ID', validators=[DataRequired("This field is required")])
    dept_name = StringField('Department Name', validators=[DataRequired("This field is required")])
