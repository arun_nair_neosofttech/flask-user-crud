from datetime import datetime, time
from bson import ObjectId
from flask import Flask, render_template, request, flash, url_for
from flask_wtf.csrf import CSRFProtect
from werkzeug.utils import redirect
from extensions import mongo
from forms import UserForm, DeptForm
from settings import SECRET_KEY

app = Flask(__name__)
csrf = CSRFProtect(app)
app.config['SECRET_KEY'] = SECRET_KEY
db = mongo['user_db']


@app.route('/')
@app.route('/index')
def index():
    return render_template('home/home.html')


@app.route('/users', methods=['GET'])
def users():
    user_collection = db.users.find().sort("first_name")
    depts = db.depts
    deptUsers = db.deptUsers
    return render_template('users/users.html', users=user_collection, depts=depts, deptUsers=deptUsers)


@app.route('/add_user', methods=['GET', 'POST'])
def add_user():
    form = UserForm()
    if request.method == 'POST':
        # import pdb
        # pdb.set_trace()
        if form.validate_on_submit():
            user = db.users.insert_one({"first_name": form.first_name.data,
                                        "last_name": form.last_name.data,
                                        "username": form.username.data,
                                        "dob": datetime.strptime(form.dob.data, '%m/%d/%Y')})
            db.deptUsers.insert_one({"user_id": user.inserted_id,
                                     "dept_id": ObjectId(form.department.data)})
            flash("User added !", "success")
            return redirect(url_for('users'))
        else:
            flash("Something went wrong, please try again !", "error")
    return render_template('users/add_user.html', form=form)


@app.route('/edit_user/<string:user_id>', methods=['GET', 'POST'])
def edit_user(user_id):
    user = db.users.find({"_id": ObjectId(user_id)})[0]
    if request.method == 'GET':
        first_name = user['first_name']
        last_name = user['last_name']
        username = user['username']
        dob = datetime.strftime(user['dob'].date(), '%m/%d/%Y')
        department = db.deptUsers.find_one({"user_id": ObjectId(user_id)})["dept_id"].__str__()
        form = UserForm(first_name=first_name, last_name=last_name, username=username, dob=dob, department=department)
        return render_template('users/update_user.html', form=form, user=user)

    elif request.method == 'POST':
        form = UserForm()
        if form.validate_on_submit():
            db.users.update_one({"_id": ObjectId(user_id)}, {"$set": {"first_name": form.first_name.data,
                                                                      "last_name": form.last_name.data,
                                                                      "username": form.username.data,
                                                                      "dob": datetime.strptime(form.dob.data,
                                                                                               '%m/%d/%Y')}})
            db.deptUsers.update_one({"user_id": ObjectId(user_id)},
                                    {"$set": {"dept_id": ObjectId(form.department.data)}})
            flash("User updated !", "success")
        else:
            flash("Something went wrong, please try again !", "error")
    return redirect(url_for('users'))


@app.route('/delete_user/<string:user_id>', methods=['GET'])
def delete_user(user_id):
    user = db.users.find({"_id": user_id})
    if user.count:
        deleted = db.users.delete_one({"_id": ObjectId(user_id)})
        db.deptUsers.delete_one({"user_id": ObjectId(user_id)})
        flash("User deleted: {}".format(deleted.deleted_count), "success")
    else:
        flash("Something went wrong !", "error")
    return redirect(url_for('users'))


@app.route('/departments', methods=['GET'])
def departments():
    dept_collection = db.depts.find().sort("dept_id")
    return render_template('departments/depts.html', depts=dept_collection)


@app.route('/add_dept', methods=['GET', 'POST'])
def add_dept():
    form = DeptForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            db.depts.insert_one({"dept_id": form.dept_id.data,
                                 "dept_name": form.dept_name.data})
            flash("Department added !", "success")
            return redirect('departments')
        else:
            flash("Something went wrong, please try again !", "error")
    return render_template('departments/add_dept.html', form=form)


@app.route('/edit_dept/<string:dept_id>', methods=['GET', 'POST'])
def edit_dept(dept_id):
    dept = db.depts.find({"_id": ObjectId(dept_id)})[0]
    if request.method == 'GET':
        dept_id = dept['dept_id']
        dept_name = dept['dept_name']
        form = DeptForm(dept_id=dept_id, dept_name=dept_name)
        return render_template('departments/update_dept.html', form=form, dept=dept)

    elif request.method == 'POST':
        form = DeptForm()
        if form.validate_on_submit():
            db.depts.update_one({"_id": ObjectId(dept_id)}, {"$set": {"dept_id": form.dept_id.data,
                                                                      "dept_name": form.dept_name.data}})
            flash("Department updated !", "success")
        else:
            flash("Something went wrong, please try again !", "error")
        return redirect(url_for('departments'))


@app.route('/delete_dept/<string:dept_id>', methods=['GET'])
def delete_dept(dept_id):
    dept = db.depts.find({"_id": dept_id})
    if dept.count:
        dept_deleted = db.depts.delete_one({"_id": ObjectId(dept_id)})
        db.deptUsers.update_many({"dept_id": ObjectId(dept_id)}, {"$set": {"dept_id": "None"}})
        flash("Department deleted: {}".format(dept_deleted.deleted_count), "success")
        return redirect(url_for('departments'))
    flash("Something went wrong !", "error")
    return redirect(url_for('departments'))
